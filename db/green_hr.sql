-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 12, 2023 at 08:16 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `green_hr`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_05_26_145836_create_permission_tables', 1),
(6, '2023_05_26_150238_create_products_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(2, 'role-create', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(3, 'role-edit', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(4, 'role-delete', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(5, 'product-list', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(6, 'product-create', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(7, 'product-edit', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(8, 'product-delete', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(9, 'menu-list', 'web', NULL, NULL),
(10, 'user-list', 'web', NULL, NULL),
(11, 'user-create', 'web', NULL, NULL),
(12, 'user-edit', 'web', NULL, NULL),
(13, 'user-delete', 'web', NULL, NULL),
(14, 'menu-create', 'web', NULL, NULL),
(15, 'menu-edit', 'web', NULL, NULL),
(16, 'menu-delete', 'web', NULL, NULL),
(17, 'home-list', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'asa', 'saas', NULL, NULL),
(2, 'AdsaDa', 'daDasd', NULL, NULL),
(3, 'asa', 'saas', NULL, NULL),
(4, 'AdsaDa', 'daDasd', NULL, NULL),
(5, 'sdavsacsac', 'sacdcascsac', '2023-05-26 09:09:46', '2023-05-26 09:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2023-05-26 08:14:11', '2023-05-26 08:14:11'),
(2, 'super admin', 'web', '2023-05-26 09:47:56', '2023-05-26 09:47:56');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 1),
(17, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('Pto0laSIdVuTUHErpHT8kSCVpzd1VyXLEMI8GYU0', 2, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoibzh3dWRZSjJnWnNwV3BzaTJDV2R2WkdBOEtiSEozdHpsVEtPV0VreCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjY6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MjtzOjQ6ImF1dGgiO2E6MTp7czoyMToicGFzc3dvcmRfY29uZmlybWVkX2F0IjtpOjE2ODYyMzMzNzA7fX0=', 1686237929);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_application_settings`
--

CREATE TABLE `tbl_application_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `apps_title` varchar(100) DEFAULT NULL,
  `apps_address` varchar(255) DEFAULT NULL,
  `apps_phone` varchar(100) DEFAULT NULL,
  `apps_email` varchar(100) DEFAULT NULL,
  `apps_id` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_application_settings`
--

INSERT INTO `tbl_application_settings` (`id`, `apps_title`, `apps_address`, `apps_phone`, `apps_email`, `apps_id`, `created_at`, `updated_at`) VALUES
(1, 'Green HR', 'Phnom Phen', '010464537', 'application@gmail.com', 'bBcdPUgATkO0DHOY63GE6n9sLEU4QK', '2023-06-07 08:32:14', '2023-06-07 08:32:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_award_form`
--

CREATE TABLE `tbl_award_form` (
  `id` int(11) UNSIGNED NOT NULL,
  `award_name` varchar(100) DEFAULT NULL,
  `award_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `award_description` text DEFAULT NULL,
  `award_gift_item` varchar(255) DEFAULT NULL,
  `award_employee_name` varchar(100) DEFAULT NULL,
  `award_by` varchar(255) DEFAULT NULL,
  `award_id` varchar(100) DEFAULT NULL,
  `award_action` tinyint(1) NOT NULL DEFAULT 1,
  `award_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_award_form`
--

INSERT INTO `tbl_award_form` (`id`, `award_name`, `award_date`, `award_description`, `award_gift_item`, `award_employee_name`, `award_by`, `award_id`, `award_action`, `award_status`, `created_at`, `updated_at`) VALUES
(1, 'csacdxxx', '2023-06-07 01:19:43', 'csacdasxx', 'xxxxcsacd', 'csacxxxx', 'csadcsaxxx', 'F4TBWif7mLV4V7sWbkI3YyakJrc4i3', 1, 1, '2023-06-07 07:54:06', '2023-06-07 01:19:43'),
(2, 'cascdas', '2023-06-07 07:54:06', 'csad', 'csacdas', 'csacdd', 'csacdas', 'csad', 1, 1, '2023-06-07 07:54:06', '2023-06-07 07:54:06'),
(3, 'acsds', '2023-06-07 01:11:48', 'scadasc', 'scacdas', 'csacd', 'csacdascd', 'Wt4CUkufsl9rXGe7FwvTq71PgwXuDU', 1, 1, '2023-06-07 01:11:48', '2023-06-07 08:11:48'),
(4, 'xxx', '2023-06-07 01:11:59', 'xxx', 'xx', 'xxx', 'xxxx', 'BrP3toYrP1FQgg1DrO7RK8N1taFVib', 1, 1, '2023-06-07 01:11:59', '2023-06-07 08:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banks`
--

CREATE TABLE `tbl_banks` (
  `id` int(11) UNSIGNED NOT NULL,
  `b_id` varchar(255) DEFAULT NULL,
  `b_name` varchar(100) DEFAULT NULL,
  `b_account_name` varchar(100) DEFAULT NULL,
  `b_account_number` varchar(100) DEFAULT NULL,
  `b_branch_name` varchar(100) DEFAULT NULL,
  `b_notice` text DEFAULT NULL,
  `b_action` tinyint(1) NOT NULL DEFAULT 1,
  `b_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_banks`
--

INSERT INTO `tbl_banks` (`id`, `b_id`, `b_name`, `b_account_name`, `b_account_number`, `b_branch_name`, `b_notice`, `b_action`, `b_status`, `created_at`, `updated_at`) VALUES
(1, 'hmojcmaaUYv6VtNHhexdYPWfqrJRlU', 'ABC Bank', 'ABC Clib', '1412412442', 'BOKOR', 'HR', 1, 1, '2023-06-03 03:23:10', '2023-06-03 10:23:10'),
(2, 'aq3xoNrLtyWUB8MfOQPtY6TYdLi1HQ', 'BCC Bank', 'KBBC', '0001-02020-21291020', 'Khompong Chham', 'HR', 1, 1, '2023-06-03 03:36:40', '2023-06-03 10:36:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_select`
--

CREATE TABLE `tbl_candidate_select` (
  `id` int(11) UNSIGNED NOT NULL,
  `cd_id` varchar(255) NOT NULL,
  `cd_name` varchar(100) NOT NULL,
  `cd_position` varchar(100) NOT NULL,
  `cd_selection_terms` varchar(255) DEFAULT NULL,
  `cd_action` tinyint(1) NOT NULL DEFAULT 1,
  `cd_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_candidate_select`
--

INSERT INTO `tbl_candidate_select` (`id`, `cd_id`, `cd_name`, `cd_position`, `cd_selection_terms`, `cd_action`, `cd_status`, `created_at`, `updated_at`) VALUES
(1, 'WnaeRzz6ZvLKTu3h5nmKYnxf7jmQQD', 'Sock', 'Assistance', 'Network', 1, 1, '2023-06-03 04:17:22', '2023-06-03 11:17:22'),
(2, 'rgsnZIugcOQvu2OBZez16ix7V7FQpG', 'John.M', 'Programmer', 'DevOP', 1, 1, '2023-06-03 04:51:54', '2023-06-03 11:51:54'),
(3, 't9e63UGuFcQCl3K1z26h0RtRU0cscS', 'sadcas', 'csa', 'csacd', 1, 1, '2023-06-03 05:00:24', '2023-06-03 12:00:24'),
(4, 'vtV9YxIqlad1MlSpzXtjGN2c3HivWi', 'sdvvsa', 'savdas', 'vsav', 1, 1, '2023-06-03 05:01:06', '2023-06-03 12:01:06'),
(5, 'mIhzEGaaCmvaiSXXDW0n6ADVR1LKOH', 'dcdas', 'dascac', 'csacds', 1, 1, '2023-06-03 05:05:59', '2023-06-03 12:05:59'),
(6, 'g4pxFZpzxafBGvfB0y6M5qwu5EFz5S', 'ascas', 'scacda', 'csacd', 1, 1, '2023-06-03 05:07:31', '2023-06-03 12:07:31'),
(7, 'LB7ZpvLlqxQgfMwNRmGYmaf09rvk5e', 'ascas', 'scacda', 'csacd', 1, 1, '2023-06-03 05:08:21', '2023-06-03 12:08:21'),
(8, 'SVYWlt6iYaKrgOOLQQvOCyMPnm47TB', 'ascad', 'sacdsa', 'csacd', 1, 1, '2023-06-03 05:09:03', '2023-06-03 12:09:03'),
(9, '4TlKG5BLozDnnwjkv7Hpq8UKT8u4Ag', 'Candidate Name', 'Candidate Name', 'Candidate Name', 1, 1, '2023-06-03 08:07:21', '2023-06-03 15:07:21'),
(10, 'wpEBDv5Z9S82MaiuTZYj1JLFukmKIt', 'w. jonh', 'Devop', 'java and docker', 1, 1, '2023-06-03 08:10:07', '2023-06-03 15:10:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_short_list`
--

CREATE TABLE `tbl_candidate_short_list` (
  `id` int(11) UNSIGNED NOT NULL,
  `csl_id` varchar(255) DEFAULT NULL,
  `csl_name` varchar(100) DEFAULT NULL,
  `csl_job_postion` varchar(100) DEFAULT NULL,
  `csl_shortlist_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `csl_interview_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `csl_action` tinyint(1) NOT NULL DEFAULT 1,
  `csl_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_candidate_short_list`
--

INSERT INTO `tbl_candidate_short_list` (`id`, `csl_id`, `csl_name`, `csl_job_postion`, `csl_shortlist_date`, `csl_interview_date`, `csl_action`, `csl_status`, `created_at`, `updated_at`) VALUES
(1, 'wkeKLeJNol1okNfsRctOxDv9A2gCR2', 'Home', 'Develop', '2023-06-03 15:23:24', '2023-06-03 15:23:24', 1, 1, '2023-06-03 15:23:24', '2023-06-03 08:39:32'),
(2, 'poDfc8W6UNbVHEhyaN6eRrqvBVOwwM', 'Lab', 'top', '2023-06-03 15:23:24', '2023-06-03 15:23:24', 1, 1, '2023-06-03 15:23:24', '2023-06-03 08:40:42'),
(3, 'wkeKLeJNol1okNfsRctOxDv9A2gCR2', 'Home', 'Develop', '2023-06-03 15:23:24', '2023-06-03 15:23:24', 1, 1, '2023-06-03 15:23:26', '2023-06-03 08:39:32'),
(4, 'poDfc8W6UNbVHEhyaN6eRrqvBVOwwM', 'Lab', 'top', '2023-06-03 15:23:24', '2023-06-03 15:23:24', 1, 1, '2023-06-03 15:23:26', '2023-06-03 08:40:42'),
(5, 'qdj9MHISR9p3ee6YuwxPzN2vAaJvMI', 'Kong', 'lab', '2023-06-03 15:23:24', '2023-06-03 15:23:24', 1, 1, '2023-06-03 08:33:06', '2023-06-03 08:41:36'),
(6, 'okbpEZGSY3vqpV2QXXPObGEgfaQORC', 'xx', 'xxx', '2023-06-03 15:23:24', '2023-06-03 15:23:24', 1, 1, '2023-06-03 08:33:18', '2023-06-03 08:47:20'),
(7, 'akBH7Lb8EzqO6lxWVzF8ApwfWe7ZOc', 'Chenda', 'NOC', '2023-06-03 15:23:24', '2023-06-04 15:23:24', 1, 1, '2023-06-03 08:46:12', '2023-06-03 08:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contacts`
--

CREATE TABLE `tbl_contacts` (
  `id` int(11) UNSIGNED NOT NULL,
  `con_branch` varchar(100) DEFAULT NULL,
  `con_address` varchar(255) DEFAULT NULL,
  `con_phone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_interviews`
--

CREATE TABLE `tbl_interviews` (
  `id` int(11) UNSIGNED NOT NULL,
  `in_name` varchar(100) DEFAULT NULL,
  `in_job_position` varchar(100) DEFAULT NULL,
  `in_date` varchar(100) DEFAULT NULL,
  `in_interviewer` varchar(100) DEFAULT NULL,
  `in_viva_marks` varchar(255) DEFAULT NULL,
  `in_wt_marks` varchar(255) DEFAULT NULL,
  `in_mt_marks` varchar(255) NOT NULL,
  `in_total_marks` varchar(255) NOT NULL,
  `in_recommand` varchar(255) DEFAULT NULL,
  `in_selection` varchar(100) DEFAULT NULL,
  `in_details` text DEFAULT NULL,
  `in_action` tinyint(1) NOT NULL DEFAULT 1,
  `int_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notice_board`
--

CREATE TABLE `tbl_notice_board` (
  `id` int(11) UNSIGNED NOT NULL,
  `n_id` varchar(255) NOT NULL,
  `n_type` varchar(255) NOT NULL,
  `n_description` text NOT NULL,
  `n_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `n_attachment` varchar(255) DEFAULT NULL,
  `n_notice_by` text DEFAULT NULL,
  `n_status` tinyint(1) DEFAULT 1,
  `n_action` tinyint(1) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_notice_board`
--

INSERT INTO `tbl_notice_board` (`id`, `n_id`, `n_type`, `n_description`, `n_date`, `n_attachment`, `n_notice_by`, `n_status`, `n_action`, `created_at`, `updated_at`) VALUES
(1, 'klp8d35BQirQ7yroPfsHOHzQPgLC7Q', 'Covid 19 Bata', 'Covid 19 2020 until 2023 or now', '2023-06-03 01:39:03', 'swdsafs', 'HR', 1, 1, '2023-06-03 07:38:12', '2023-06-03 01:39:03'),
(2, 'ZDrJFkESDxsgcJWjqRdqdS5TNHxFqH', 'Covid 19', 'Covid 19 2020 until 2023 or now', '2023-06-03 01:38:01', 'swdsafs', 'HR', 1, 1, '2023-06-03 07:38:16', '2023-06-03 01:38:01'),
(3, 'gShsASXGotW5cg85eY4cE8ToW661Kc', 'Version Covid19 Bate', 'on 2022', '2023-06-03 01:49:13', 'Note', 'HR', 1, 1, '2023-06-03 01:11:00', '2023-06-03 01:49:13'),
(4, 'ilCCX4USxR5IKvE3EA4Ev8vvbmbfd5', 'Covid 19 finish', 'on 2023', '2023-06-03 01:48:12', 'Note', 'HR', 1, 1, '2023-06-03 01:48:12', '2023-06-03 08:48:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_positions`
--

CREATE TABLE `tbl_positions` (
  `id` int(11) UNSIGNED NOT NULL,
  `p_name` varchar(255) DEFAULT NULL,
  `p_action` tinyint(1) NOT NULL DEFAULT 1,
  `p_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hardik Savani', 'admin@gmail.com', NULL, '$2y$10$Ii1uW.eGnZRCrVKQZdhmuuSeWcT/fieWeNAJ0NjZrkW3ab5T1lxtq', NULL, '2023-05-26 08:14:11', '2023-05-26 08:14:11'),
(2, 'jonh', 'jonh@gmail.com', NULL, '$2y$10$FrqjiH1SbyceJx5JEv1ILeI0UGJKfm9Qv37d/pM3Dv6sWEAos5w2S', NULL, '2023-05-26 09:48:48', '2023-05-26 09:48:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `tbl_application_settings`
--
ALTER TABLE `tbl_application_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_award_form`
--
ALTER TABLE `tbl_award_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_banks`
--
ALTER TABLE `tbl_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_candidate_select`
--
ALTER TABLE `tbl_candidate_select`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_candidate_short_list`
--
ALTER TABLE `tbl_candidate_short_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_interviews`
--
ALTER TABLE `tbl_interviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notice_board`
--
ALTER TABLE `tbl_notice_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_positions`
--
ALTER TABLE `tbl_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_application_settings`
--
ALTER TABLE `tbl_application_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_award_form`
--
ALTER TABLE `tbl_award_form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_banks`
--
ALTER TABLE `tbl_banks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_candidate_select`
--
ALTER TABLE `tbl_candidate_select`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_candidate_short_list`
--
ALTER TABLE `tbl_candidate_short_list`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_contacts`
--
ALTER TABLE `tbl_contacts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_interviews`
--
ALTER TABLE `tbl_interviews`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_notice_board`
--
ALTER TABLE `tbl_notice_board`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_positions`
--
ALTER TABLE `tbl_positions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
