<?php
    
namespace App\Http\Controllers;
    
use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class CandidateShortlistController extends Controller
{ 
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:menu-list|menu-create|menu-edit|menu-delete');
        //  $this->middleware('permission:menu-list', ['only' => ['index','show']]);
        //  $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidateShortlist = DB::
        table('tbl_candidate_short_list')
        ->where('csl_action','=',1)
        ->paginate(5);
        return view('candidate_short_list.index',compact('candidateShortlist'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidate_short_list.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 30; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
        DB::
       table('tbl_candidate_short_list')
        ->insert([
            'csl_id' => $randomString,
            'csl_name' => $request->csl_name,
            'csl_job_postion' => $request->csl_job_postion,
            'csl_shortlist_date' => $request->csl_shortlist_date,
            'csl_interview_date' => $request->csl_interview_date,
            'created_at' => new \DateTime(),
        ]);
    



        
        return redirect()->route('candidate-short-list');
                        //->with('success','Product created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($csl_id)
    {
        $candidateShortlist = DB::
        table('tbl_candidate_short_list')
        ->where('csl_id','=', $csl_id)
        ->first();
        
        return view('candidate_short_list.show',compact('candidateShortlist'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($csl_id)
    {
        $candidateShortlist = DB::
        table('tbl_candidate_short_list')
        ->where('csl_id','=', $csl_id)
        ->first();

   
        return view('candidate_short_list.edit',compact('candidateShortlist'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$csl_id)
    {
        //  request()->validate([
        //     'name' => 'required',
        //     'guard_name' => 'required',
        // ]);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 30; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        
        DB::
        table('tbl_candidate_short_list')
        ->where('csl_id', $csl_id)
        ->update([
            'csl_id' => $randomString,
            'csl_name' => $request->csl_name,
            'csl_job_postion' => $request->csl_job_postion,
            'csl_shortlist_date' => $request->csl_shortlist_date,
            'csl_interview_date' => $request->csl_interview_date,
            'updated_at' => new \DateTime(),
        ]);
    
        return redirect()->route('candidate-short-list');
                        //->with('success','Product updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    
        return redirect()->route('candidate-short-list');
                        ///->with('success','Product deleted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Product $product)
    {
        $product->delete();
    
        return redirect()->route('candidate-short-list');
                        ///->with('success','Product deleted successfully');
    }

}