<?php
    
namespace App\Http\Controllers;
    
use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class CandidateSelectController extends Controller
{ 
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:menu-list|menu-create|menu-edit|menu-delete');
        //  $this->middleware('permission:menu-list', ['only' => ['index','show']]);
        //  $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidateSelect = DB::
        table('tbl_candidate_select')
        ->where('cd_action','=',1)
        ->paginate(5);

        return view('candidate_select.index',compact('candidateSelect'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidate_select.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 30; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
        DB::
       table('tbl_candidate_select')
        ->insert([
            'cd_id' => $randomString,
            'cd_name' => $request->cd_name,
            'cd_position' => $request->cd_position,
            'cd_selection_terms' => $request->cd_selection_terms,
            'created_at' => new \DateTime(),
        ]);
    
        
        return redirect()->route('candidate-select');
                        //->with('success','Product created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($cd_id)
    {
        $candidateSelect = DB::
        table('tbl_candidate_select')
        ->where('cd_id','=', $cd_id)
        ->first();
        
        return view('candidate_select.show',compact('candidateSelect'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($cd_id)
    {
        $candidateSelect = DB::
        table('tbl_candidate_select')
        ->where('cd_id','=', $cd_id)
        ->first();

   
        return view('candidate_select.edit',compact('candidateSelect'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$cd_id)
    {
        //  request()->validate([
        //     'name' => 'required',
        //     'guard_name' => 'required',
        // ]);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 30; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        
        DB::
        table('tbl_candidate_select')
        ->where('cd_id', $cd_id)
        ->update([
            'cd_id' => $randomString,
            'cd_name' => $request->cd_name,
            'cd_position' => $request->cd_position,
            'cd_selection_terms' => $request->cd_selection_terms,
        ]);
    
        return redirect()->route('candidate-select');
                        //->with('success','Product updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    
        return redirect()->route('candidate-select');
                        ///->with('success','Product deleted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

}