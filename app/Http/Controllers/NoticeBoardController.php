<?php
    
namespace App\Http\Controllers;
    
use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class NoticeBoardController extends Controller
{ 
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:menu-list|menu-create|menu-edit|menu-delete');
        //  $this->middleware('permission:menu-list', ['only' => ['index','show']]);
        //  $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticeBoard = DB::
        table('tbl_notice_board')
        ->where('n_action','=',1)
        ->paginate(5);
        return view('notice_board.index',compact('noticeBoard'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notice_board.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 30; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
        DB::
       table('tbl_notice_board')
        ->insert([
            'n_id' => $randomString,
            'n_type' => $request->n_type,
            'n_description' => $request->n_description,
            'n_date' => new \DateTime(),
            'n_attachment' => $request->n_attachment,
            'n_notice_by' => $request->n_notice_by,
            'created_at' => new \DateTime(),
        ]);
    
        return redirect()->route('notice-board');
                        //->with('success','Product created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($n_id)
    {
        $noticeBoard = DB::
        table('tbl_notice_board')
        ->where('n_id','=', $n_id)
        ->first();
        
        return view('notice_board.show',compact('noticeBoard'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($n_id)
    {
        $noticeBoard = DB::
        table('tbl_notice_board')
        ->where('n_id','=', $n_id)
        ->first();

   
        return view('notice_board.edit',compact('noticeBoard'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$n_id)
    {
        //  request()->validate([
        //     'name' => 'required',
        //     'guard_name' => 'required',
        // ]);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 30; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        
        DB::
        table('tbl_notice_board')
        ->where('n_id', $n_id)
        ->update([
            'n_id' => $randomString,
            'n_type' => $request->n_type,
            'n_description' => $request->n_description,
            'n_date' => new \DateTime(),
            'n_attachment' => $request->n_attachment,
            'n_notice_by' => $request->n_notice_by,
            'updated_at' => new \DateTime(),
        ]);
    
        return redirect()->route('notice-board');
                        //->with('success','Product updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

}