<?php
    
namespace App\Http\Controllers;
    
use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class BankController extends Controller
{ 
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:menu-list|menu-create|menu-edit|menu-delete');
        //  $this->middleware('permission:menu-list', ['only' => ['index','show']]);
        //  $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bank = DB::
        table('tbl_banks')
        ->where('b_action','=',1)
        ->paginate(5);
        return view('bank.index',compact('bank'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bank.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 30; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
        DB::
       table('tbl_banks')
        ->insert([
            'b_id' => $randomString,
            'b_name' => $request->b_name,
            'b_account_name' => $request->b_account_name,
            'b_account_number' => $request->b_account_number,
            'b_branch_name' => $request->b_branch_name,
            'b_notice' => $request->b_notice,
            'created_at' => new \DateTime(),
        ]);
    
        return redirect()->route('bank');
                        //->with('success','Product created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($b_id)
    {
        $bank = DB::
        table('tbl_banks')
        ->where('b_id','=', $b_id)
        ->first();
        
        return view('bank.show',compact('bank'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($b_id)
    {
        $bank = DB::
        table('tbl_banks')
        ->where('b_id','=', $b_id)
        ->first();

   
        return view('bank.edit',compact('bank'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$b_id)
    {
        //  request()->validate([
        //     'name' => 'required',
        //     'guard_name' => 'required',
        // ]);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 30; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        
        DB::
        table('tbl_banks')
        ->where('b_id', $b_id)
        ->update([
            'b_id' => $randomString,
            'b_name' => $request->b_name,
            'b_account_name' => $request->b_account_name,
            'b_account_number' => $request->b_account_number,
            'b_branch_name' => $request->b_branch_name,
            'b_notice' => $request->b_notice,
        ]);
    
        return redirect()->route('bank');
                        //->with('success','Product updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

}