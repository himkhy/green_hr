<?php
    
namespace App\Http\Controllers;
    
use App\Models\Product;
use Illuminate\Http\Request;
use DB;

class  ApplicationSettingController extends Controller
{ 
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:menu-list|menu-create|menu-edit|menu-delete');
        //  $this->middleware('permission:menu-list', ['only' => ['index','show']]);
        //  $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applicationSetting = DB::
        table('tbl_application_settings')
        ->first();
        return view('application_setting.edit',compact('applicationSetting'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('award_form.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 30; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
        DB::
       table('tbl_award_form')
        ->insert([
            'award_id' => $randomString,
            'award_name' => $request->award_name,
            'award_description' => $request->award_description,
            'award_date' => new \DateTime(),
            'award_gift_item' => $request->award_gift_item,
            'award_employee_name' => $request->award_employee_name,
            'award_by' => $request->award_by,
            'created_at' => new \DateTime(),
        ]);

        return redirect()->route('award-form');
                        //->with('success','Product created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($award_id)
    {
        $awardForm = DB::
        table('tbl_award_form')
        ->where('award_id','=', $award_id)
        ->first();
        
        return view('award_form.show',compact('awardForm'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($award_id)
    {
        $awardForm = DB::
        table('tbl_award_form')
        ->where('award_id','=', $award_id)
        ->first();

   
        return view('award_form.edit',compact('awardForm'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$apps_id)
    {
        //  request()->validate([
        //     'name' => 'required',
        //     'guard_name' => 'required',
        // ]);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 30; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        
        DB::
        table('tbl_application_settings')
        ->where('apps_id', $apps_id)
        ->update([
            'apps_id' => $randomString,
            'apps_title' => $request->apps_title,
            'apps_address' => $request->apps_address,
            'apps_phone' => $request->apps_phone,
            'apps_email' =>  $request->apps_email,
            
        ]);
    
        return redirect()->route('application-setting');
                        //->with('success','Product updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Product $product)
    {
        $product->delete();
    
        return redirect()->route('notice-board');
                        ///->with('success','Product deleted successfully');
    }

}