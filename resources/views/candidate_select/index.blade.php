@extends('layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 1604.44px;">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Candidate Select Management</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active"> Candidate Select Management Page</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                    
                        <div class="pull-right">
                            {{-- @can('product-create') --}}
                            <a class="btn btn-success" href="{{ route('create-candidate-select') }}"> Add New Candidate Select</a>
                            {{-- @endcan --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="content mx-2">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <tr>
                            <th>SL</th>
                            <th>Candidate Name</th>
                            <th>Candidate Id</th>
                            <th>Employee Id</th>
                            <th>Postion</th>
                            <th>Selection Terms</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($candidateSelect as $key => $candidateSelects)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $candidateSelects->cd_name }}</td>
                            <td>32343243423424234</td>
                            <td>234</td>
                            <td>{{ $candidateSelects->cd_position }}</td>
                            <td>{{ $candidateSelects->cd_selection_terms }}</td>
                            <td class="text-center align-right">
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('change-status-candidate-select', ['cd_id' => $candidateSelects->cd_id, 'st' => $candidateSelects->cd_status == 0? 1:0 ]) }}" class="btn {{ $candidateSelects->cd_status == 0? 'btn-danger' : 'btn-success' }}">{!!$candidateSelects->cd_status == 0? '<i class="fas fa-minus-circle"></i>' : '<i class="fas fa-check-circle"></i>'!!}</a>
                                    <a href="{{ route('show-candidate-select', ['cd_id' => $candidateSelects->cd_id]) }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('edit-candidate-select', ['cd_id' => $candidateSelects->cd_id]) }}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                    <a href="{{ route('destroy-candidate-select', ['cd_id' => $candidateSelects->cd_id]) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                
                    <br>
                    {!! $candidateSelect->links('pagination::bootstrap-4') !!}
                
                </div>

                <div class="card-footer">
                    Footer
                </div>

            </div>

        </section>

    </div>

    


   


 


@endsection
