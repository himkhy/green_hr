@extends('layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 1604.44px;">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Award Form Management</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Award Form Management Page</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                    
                        <div class="pull-right">
                            {{-- @can('product-create') --}}
                            <a class="btn btn-success" href="{{ route('create-award-form') }}"> Add New Notice Board</a>
                            {{-- @endcan --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="content mx-2">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Award Name</th>
                            <th>Award Description</th>
                            <th>Gift Item</th>
                            <th>Date</th>
                            <th>Employee Name</th>
                            <th>Award By</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($awardForm as $key => $awardForms)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $awardForms->award_name }}</td>
                            <td>{{ $awardForms->award_description }}</td>
                            <td>{{ $awardForms->award_gift_item }}</td>
                            <td>{{ $awardForms->award_date }}</td>
                            <td>{{ $awardForms->award_employee_name }}</td>
                            <td>{{ $awardForms->award_by }}</td>
                            <td class="text-center align-right">
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('change-status-award-form', ['award_id' => $awardForms->award_id, 'st' => $awardForms->award_status == 0? 1:0 ]) }}" class="btn {{ $awardForms->award_status == 0? 'btn-danger' : 'btn-success' }}">{!!$awardForms->award_status == 0? '<i class="fas fa-minus-circle"></i>' : '<i class="fas fa-check-circle"></i>'!!}</a>
                                    <a href="{{ route('show-award-form', ['award_id' => $awardForms->award_id]) }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('edit-award-form', ['award_id' => $awardForms->award_id]) }}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                    <a href="{{ route('destroy-award-form', ['award_id' => $awardForms->award_id]) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                
                    <br>
                    {!! $awardForm->links('pagination::bootstrap-4') !!}
                
                </div>

                <div class="card-footer">
                    Footer
                </div>

            </div>

        </section>

    </div>

    


   


 


@endsection
