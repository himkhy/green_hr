




@extends('layouts.app')
@section('content')
    <div class="content-wrapper" >

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Application Setting Management</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Application Setting Management Page</li>
                        </ol>
                    </div>
                </div>

        

            </div>
        </section>
      
        <section class="content mx-2">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                  
                    <form action="{{ route('update-application-setting',$applicationSetting->apps_id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                    
                        
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Application Title:</strong>
                                    <input type="text" name="apps_title" value="{{$applicationSetting->apps_title}}" class="form-control" placeholder="Name">
                                </div>
                            </div>
            
                          
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Address:</strong>
                                    <input type="text" name="apps_address" value="{{$applicationSetting->apps_address}}"  class="form-control" placeholder="Name">
                                </div>
                            </div>
                
                         
                            
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Phone:</strong>
                                    <input type="text" name="apps_phone" value="{{$applicationSetting->apps_phone}}"  class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Email:</strong>
                                    <input type="text" name="apps_email" value="{{$applicationSetting->apps_email}}"  class="form-control" placeholder="Name">
                                </div>
                            </div>
                          

            		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>
                    
                    </form>

            
                </div>

                <div class="card-footer">
                    Footer
                </div>

            </div>

        </section>

    </div>

@endsection