




@extends('layouts.app')
@section('content')
    <div class="content-wrapper" >

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit  Notice Board Management</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit  Notice Board Management Page</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                  <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('notice-board') }}"> Back</a>
                    </div>
                  </div>
              </div>
              

            </div>
        </section>
      
        <section class="content mx-2">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                  
                    <form action="{{ route('update-notice-board',$noticeBoard->n_id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                    
                        
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Notice Type:</strong>
                                    <input type="text" value="{{$noticeBoard->n_type}}" name="n_type" class="form-control" placeholder="Name">
                                </div>
                            </div>
                
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Description:</strong>
                                    <textarea class="form-control" 
                                     style="height:150px" name="n_description" placeholder="Detail">{{$noticeBoard->n_description}}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Notice Date:</strong>
                                    <input type="text" name="n_date" value="{{$noticeBoard->n_date}}" class="form-control" placeholder="Name">
                                </div>
                            </div>
                
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Attachment:</strong>
                                    <input type="text" name="n_attachment" value="{{$noticeBoard->n_attachment}}" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Notice By:</strong>
                                    <input type="text" name="n_notice_by" value="{{$noticeBoard->n_notice_by}}" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    
                    
                    </form>

            
                </div>

                <div class="card-footer">
                    Footer
                </div>

            </div>

        </section>

    </div>

@endsection