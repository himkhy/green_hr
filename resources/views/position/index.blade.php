@extends('layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 1604.44px;">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Candidate Shortlist Management</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active"> Candidate Shortlist Management Page</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                    
                        <div class="pull-right">
                            {{-- @can('product-create') --}}
                            <a class="btn btn-success" href="{{ route('create-candidate-short-list') }}"> Add New Candidate Shortlist</a>
                            {{-- @endcan --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="content mx-2">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <tr>
                            <th>SL</th>
                            <th>No</th>
                            <th>Candidate Name</th>
                            <th>Candidate Id</th>
                            <th>Job Position</th>
                            <th>Shortlist Date	</th>
                            <th>Interview Date</th>
                            <th width="280px">Action</th>
                        </tr>

                        @foreach ($candidateShortlist as $key => $candidateShortlists)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>34434</td>
                            <td>16556178762808S</td>
                            <td>{{ $candidateShortlists->csl_name }}</td>
                            <td>{{ $candidateShortlists->csl_job_postion }}</td>
                            <td>{{ $candidateShortlists->csl_shortlist_date }}</td>
                            <td>{{ $candidateShortlists->csl_interview_date	}}</td>
                            <td class="text-center align-right">
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('change-status-candidate-short-list', ['csl_id' => $candidateShortlists->csl_id, 'st' => $candidateShortlists->csl_status == 0? 1:0 ]) }}" class="btn {{ $candidateShortlists->csl_status == 0? 'btn-danger' : 'btn-success' }}">{!! $candidateShortlists->csl_status== 0? '<i class="fas fa-minus-circle"></i>' : '<i class="fas fa-check-circle"></i>'!!}</a>
                                    <a href="{{ route('show-candidate-short-list', ['csl_id' => $candidateShortlists->csl_id]) }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('edit-candidate-short-list', ['csl_id' => $candidateShortlists->csl_id]) }}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                    <a href="{{ route('destroy-candidate-short-list', ['csl_id' => $candidateShortlists->csl_id]) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                
                    <br>
                    {!! $candidateShortlist->links('pagination::bootstrap-4') !!}
                
                </div>

                <div class="card-footer">
                    Footer
                </div>

            </div>

        </section>

    </div>

    


   


 


@endsection
