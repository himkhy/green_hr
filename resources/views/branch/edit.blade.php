@extends('layouts.backend')
@section('backendContent')




<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Form</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Edit From Video</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-6">

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add New</h3>
                    </div>

                    <form id="quickForm" method="POST" action="{{ route('update-branch', ['id' => $getBranch->map_id, 'branches' => $branches]) }}" enctype="multipart/form-data" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="map_title">Title<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_title" name="map_title" value="{{$getBranch->map_title}}">

                                @if ($errors->has('map_title'))
                                    <span class="text-danger">{{ $errors->first('map_title') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="map_slug">slug<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_slug" name="map_slug" value="{{$getBranch->map_slug}}">


                                @if ($errors->has('map_slug'))
                                    <span class="text-danger">{{ $errors->first('map_slug') }}</span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="map_phone">Phone<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_phone" name="map_phone" value="{{$getBranch->map_phone}}">

                                @if ($errors->has('map_phone'))
                                    <span class="text-danger">{{ $errors->first('map_phone') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="map_email">Email<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_email" name="map_email" value="{{$getBranch->map_email}}">

                                @if ($errors->has('map_email'))
                                    <span class="text-danger">{{ $errors->first('map_email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="map_day">Day<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_day" name="map_day" value="{{$getBranch->map_day}}">

                                @if ($errors->has('map_day'))
                                    <span class="text-danger">{{ $errors->first('map_day') }}</span>
                                @endif
                            </div>



                            <div class="form-group">
                                <label for="map_weekly">Weekly<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_weekly" name="map_weekly" value="{{$getBranch->map_weekly}}">

                                @if ($errors->has('map_weekly'))
                                    <span class="text-danger">{{ $errors->first('map_weekly') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="map_latitude">Latitude<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_latitude" name="map_latitude" value="{{$getBranch->map_latitude}}"
                                    >

                                @if ($errors->has('map_latitude'))
                                    <span class="text-danger">{{ $errors->first('map_latitude') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="map_longitude">Longitude<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="map_longitude" name="map_longitude" value="{{$getBranch->map_longitude}}">

                                @if ($errors->has('map_longitude'))
                                    <span class="text-danger">{{ $errors->first('map_longitude') }}</span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="map_detail">Detail<sup class="text-danger">*</sup></label>
                                <textarea id="summernote"  class="form-control" id="map_detail" rows="3" name="map_detail">{{$getBranch->map_detail}}</textarea>

                                @if ($errors->has('map_detail'))
                                    <span class="text-danger">{{ $errors->first('map_detail') }}</span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="map_order">Order<sup class="text-danger">*</sup></label>
                                <input type="number" class="form-control" id="map_order" name="map_order" value="{{$getBranch->map_order}}">

                                @if ($errors->has('map_order'))
                                    <span class="text-danger">{{ $errors->first('map_order') }}</span>
                                @endif
                            </div>


                            <div id="information-part" class="content" role="tabpanel" aria-labelledby="information-part-trigger">
                                <label for="mes_image">Select a Image: (width: 960px x height: 720px)<sup class="text-danger">*</sup></label><br>
                                @if( $getBranch->map_image == true)
                                <input type="hidden" value="{{$getBranch->map_image}}" name="old_image"><br><br>
                                @endif
                                <input type="file" id="img" name="new_image"><br><br>
                            </div>
                            <img id="sh_img" src=" {{ asset('/') }}{{$getBranch->map_image}} " alt="{{$getBranch->map_title}}" style="height: 150px; width: 150px;"><br><br>


                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" style="width: 100%;" name="map_status">
                                  @if($getBranch->map_status == 1)
                                    <option selected="selected" value="1">Active</option>
                                    <option value="0">Suspend</option>
                                  @else
                                    <option value="1">Active</option>
                                    <option selected="selected" value="0">Suspend</option>
                                  @endif
                                </select>
                            </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</section>

@endsection

@section('scripts')

   {{-- @include('partial') --}}

@endsection
