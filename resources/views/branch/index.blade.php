@extends('layouts.backend')
@section('backendContent')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Loan Article</h1>

            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Branch</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">

        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Branch Table</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <a href="{{ route('create-branch', ['branches' => $branches]) }}" class="btn btn-block btn-primary">Add New</a>

                            </div>
                        </div>
                    </div>

                    <div class="card-body ">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Order</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>



                                @foreach ($getBranch as $key=> $getBranchs)


                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$getBranchs->map_title}}</td>
                                    <td>{{$getBranchs->map_slug}}</td>
                                    <td>{{$getBranchs->map_order}}</td>

                                    <td class="text-center align-right">
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{ route('change-status-branch', ['id' => $getBranchs->map_id, 'st' => $getBranchs->map_status == 0? 1:0 , 'branches' => $branches]) }}" class="btn {{ $getBranchs->map_status == 0? 'btn-danger' : 'btn-success' }}">{!!$getBranchs->map_status == 0? '<i class="fas fa-minus-circle"></i>' : '<i class="fas fa-check-circle"></i>'!!}</a>
                                            <a href="{{ route('show-branch', ['id' => $getBranchs->map_id]) }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                            <a href="{{ route('edit-branch', ['id' => $getBranchs->map_id, 'branches' => $branches]) }}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                            <a href="{{ route('destroy-branch', ['id' => $getBranchs->map_id, 'branches' => $branches]) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                        </div>
                                    </td>

                                </tr>

                                @endforeach







                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>







    </div>
</section>


@endsection

@section('scripts')

   {{-- @include('partial') --}}

@endsection




