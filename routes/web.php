<?php
  
use Illuminate\Support\Facades\Route;
  
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\MenuController;

  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  
// Route::get('/', function () {
//     return view('welcome');
// });
  

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
  

  
Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);

    Route::resource('menus', MenuController::class);
    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of menu admin
    |
    |
    |
    */

    Route::get('/get-menu', 'App\Http\Controllers\MenuController@index')->name('get-menu');



    



    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of award form
    |
    |
    |
    */

    Route::get('/application-setting', 'App\Http\Controllers\ApplicationSettingController@index')->name('application-setting');
    // Route::get('/create-award-form', 'App\Http\Controllers\AwardController@create')->name('create-award-form');
    // Route::post('/store-award-form', 'App\Http\Controllers\AwardController@store')->name('store-award-form');
    // Route::get('/edit-award-form/{award_id?}', 'App\Http\Controllers\AwardController@edit')->name('edit-award-form');
    Route::post('update-application-setting/{apps_id?}', 'App\Http\Controllers\ApplicationSettingController@update')->name('update-application-setting');
    // Route::post('/destroy-award-form', 'App\Http\Controllers\AwardController@destroy')->name('destroy-award-form');
    // Route::get('/show-award-form/{award_id?}', 'App\Http\Controllers\AwardController@show')->name('show-award-form');
    // Route::get('/change-status-award-form/{award_id?}', 'App\Http\Controllers\AwardController@changeStatus')->name('change-status-award-form');


    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of award form
    |
    |
    |
    */

    Route::get('/award-form', 'App\Http\Controllers\AwardController@index')->name('award-form');
    Route::get('/create-award-form', 'App\Http\Controllers\AwardController@create')->name('create-award-form');
    Route::post('/store-award-form', 'App\Http\Controllers\AwardController@store')->name('store-award-form');
    Route::get('/edit-award-form/{award_id?}', 'App\Http\Controllers\AwardController@edit')->name('edit-award-form');
    Route::post('update-award-form/{award_id?}', 'App\Http\Controllers\AwardController@update')->name('update-award-form');
    Route::post('/destroy-award-form', 'App\Http\Controllers\AwardController@destroy')->name('destroy-award-form');
    Route::get('/show-award-form/{award_id?}', 'App\Http\Controllers\AwardController@show')->name('show-award-form');
    Route::get('/change-status-award-form/{award_id?}', 'App\Http\Controllers\AwardController@changeStatus')->name('change-status-award-form');


    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Notice Board
    |
    |
    |
    */

    Route::get('/notice-board', 'App\Http\Controllers\NoticeBoardController@index')->name('notice-board');
    Route::get('/create-notice-board', 'App\Http\Controllers\NoticeBoardController@create')->name('create-notice-board');
    Route::post('/store-notice-board', 'App\Http\Controllers\NoticeBoardController@store')->name('store-notice-board');
    Route::get('/edit-notice-board/{n_id?}', 'App\Http\Controllers\NoticeBoardController@edit')->name('edit-notice-board');
    Route::post('update-notice-board/{n_id?}', 'App\Http\Controllers\NoticeBoardController@update')->name('update-notice-board');
    Route::post('/destroy-notice-board', 'App\Http\Controllers\NoticeBoardController@destroy')->name('destroy-notice-board');
    Route::get('/show-notice-board/{n_id?}', 'App\Http\Controllers\NoticeBoardController@show')->name('show-notice-board');
    Route::get('/admin/change-status-notice-board/{n_id?}', 'App\Http\Controllers\NoticeBoardController@changeStatusAdmins')->name('change-status-notice-board');



    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Bank
    |
    |
    |
    */

    Route::get('/bank', 'App\Http\Controllers\BankController@index')->name('bank');
    Route::get('/create-bank', 'App\Http\Controllers\BankController@create')->name('create-bank');
    Route::post('/store-bank', 'App\Http\Controllers\BankController@store')->name('store-bank');
    Route::get('/edit-bank/{b_id?}', 'App\Http\Controllers\BankController@edit')->name('edit-bank');
    Route::post('update-bank/{b_id?}', 'App\Http\Controllers\BankController@update')->name('update-bank');
    Route::post('/destroy-bank', 'App\Http\Controllers\BankController@destroy')->name('destroy-bank');
    Route::get('/show-bank/{b_id?}', 'App\Http\Controllers\BankController@show')->name('show-bank');
    Route::get('/admin/change-status-bank/{b_id?}', 'App\Http\Controllers\BankController@changeStatusAdmins')->name('change-status-bank');


    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Candidate Select
    |
    |
    |
    */

    Route::get('/candidate-select', 'App\Http\Controllers\CandidateSelectController@index')->name('candidate-select');
    Route::get('/create-candidate-select', 'App\Http\Controllers\CandidateSelectController@create')->name('create-candidate-select');
    Route::post('/store-candidate-select', 'App\Http\Controllers\CandidateSelectController@store')->name('store-candidate-select');
    Route::get('/edit-candidate-select/{cd_id?}', 'App\Http\Controllers\CandidateSelectController@edit')->name('edit-candidate-select');
    Route::post('update-candidate-select/{cd_id?}', 'App\Http\Controllers\CandidateSelectController@update')->name('update-candidate-select');
    Route::post('/destroy-candidate-select', 'App\Http\Controllers\CandidateSelectController@destroy')->name('destroy-candidate-select');
    Route::get('/show-candidate-select/{cd_id?}', 'App\Http\Controllers\CandidateSelectController@show')->name('show-candidate-select');
    Route::get('/admin/change-status-candidate-select/{cd_id?}', 'App\Http\Controllers\CandidateSelectController@changeStatusAdmins')->name('change-status-candidate-select');


 
    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Candidate Short List
    |
    |
    |    
    */
    Route::get('/candidate-short-list', 'App\Http\Controllers\CandidateShortlistController@index')->name('candidate-short-list');
    Route::get('/create-candidate-short-list', 'App\Http\Controllers\CandidateShortlistController@create')->name('create-candidate-short-list');
    Route::post('/store-candidate-short-list', 'App\Http\Controllers\CandidateShortlistController@store')->name('store-candidate-short-list');
    Route::get('/edit-candidate-short-list/{csl_id?}', 'App\Http\Controllers\CandidateShortlistController@edit')->name('edit-candidate-short-list');
    Route::post('update-candidate-short-list/{csl_id?}', 'App\Http\Controllers\CandidateShortlistController@update')->name('update-candidate-short-list');
    Route::post('/destroy-candidate-short-list', 'App\Http\Controllers\CandidateShortlistController@destroy')->name('destroy-candidate-short-list');
    Route::get('/show-candidate-short-list/{csl_id?}', 'App\Http\Controllers\CandidateShortlistController@show')->name('show-candidate-short-list');
    Route::get('/admin/change-status-candidate-short-list/{csl_id?}', 'App\Http\Controllers\CandidateShortlistController@changeStatusAdmins')->name('change-status-candidate-short-list');


        /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Interview
    |
    |
    |    
    */
    Route::get('/interview', 'App\Http\Controllers\InterviewController@index')->name('interview');
    Route::get('/create-interview', 'App\Http\Controllers\InterviewController@create')->name('create-interview');
    Route::post('/store-interview', 'App\Http\Controllers\InterviewController@store')->name('store-interview');
    Route::get('/edit-interview/{in_id?}', 'App\Http\Controllers\InterviewController@edit')->name('edit-interview');
    Route::post('update-interview/{in_id?}', 'App\Http\Controllers\InterviewController@update')->name('update-interview');
    Route::post('/destroy-interview', 'App\Http\Controllers\InterviewController@destroy')->name('destroy-interview');
    Route::get('/show-interview/{in_id?}', 'App\Http\Controllers\InterviewController@show')->name('show-interview');
    Route::get('/admin/change-status-interview/{in_id?}', 'App\Http\Controllers\InterviewController@changeStatusAdmins')->name('change-status-interview');



    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Position
    |
    |
    |    
    */
    Route::get('/position', 'App\Http\Controllers\PositionController@index')->name('position');
    Route::get('/create-position', 'App\Http\Controllers\PositionController@create')->name('create-position');
    Route::post('/store-position', 'App\Http\Controllers\PositionController@store')->name('store-position');
    Route::get('/edit-position/{p_id?}', 'App\Http\Controllers\PositionController@edit')->name('edit-position');
    Route::post('update-position/{p_id?}', 'App\Http\Controllers\PositionController@update')->name('update-position');
    Route::post('/destroy-position', 'App\Http\Controllers\PositionController@destroy')->name('destroy-position');
    Route::get('/show-position/{p_id?}', 'App\Http\Controllers\PositionController@show')->name('show-position');
    Route::get('/admin/change-status-position/{p_id?}', 'App\Http\Controllers\PositionController@changeStatusAdmins')->name('change-status-position');



    /*
    |--------------------------------------------------------------------------
    | Web Routes backend
    |--------------------------------------------------------------------------
    |
    | Here is routs backend controller of Position
    |
    |
    |    
    */
    Route::get('/contact', 'App\Http\Controllers\ContactController@index')->name('contact');
    Route::get('/create-contact', 'App\Http\Controllers\ContactController@create')->name('create-contact');
    Route::post('/store-contact', 'App\Http\Controllers\ContactController@store')->name('store-contact');
    Route::get('/edit-contact/{p_id?}', 'App\Http\Controllers\ContactController@edit')->name('edit-contact');
    Route::post('update-contact/{p_id?}', 'App\Http\Controllers\ContactController@update')->name('update-contact');
    Route::post('/destroy-contact', 'App\Http\Controllers\ContactController@destroy')->name('destroy-contact');
    Route::get('/show-contact/{p_id?}', 'App\Http\Controllers\ContactController@show')->name('show-contact');
    Route::get('/admin/change-status-contact/{p_id?}', 'App\Http\Controllers\ContactController@changeStatusAdmins')->name('change-status-contact');

    

    

    // Route::get('/create-admins', 'App\Http\Controllers\AdminsController@create')->name('create-admins');
    // Route::get('/edit-admins/{id?}', 'App\Http\Controllers\AdminsController@edit')->name('edit-admins');

    // Route::get('/edit-profile-admins/{id?}', 'App\Http\Controllers\AdminsController@editProfile')->name('edit-profile-admins');
    // Route::post('/update-profile-admins/{id?}', 'App\Http\Controllers\AdminsController@updateProfile')->name('update-profile-admins');
    // Route::post('update-admins/{id?}', 'App\Http\Controllers\AdminController@update')->name('update-admins');
    // Route::post('/destroy-admins', 'App\Http\Controllers\AdminsController@destroy')->name('destroy-admins');
    // Route::get('/show-admins/{id?}', 'App\Http\Controllers\AdminsController@show')->name('show-admins');
    // Route::get('/admin/change-status-admins/{id?}', 'App\Http\Controllers\AdminsController@changeStatusAdmins')->name('change-status-admins');



});